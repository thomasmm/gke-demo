#Variable Files
variable "gcp_region" {
    default = "us-central1"
}

variable "gcp_zone" {
    default = "us-central1-a"
}

variable "mysql_password" {
    default = "your mysql root password here"
}

variable "mysql_version" {
  default = "5.6"
}

variable "wordpress_version" {
  default = "5.3.2"
}
