#Provider Google
provider "google" {
  credentials = file("./.credentials/gke-demo-266622-1e65d9880e77.json")
  project = var.project_id
  region      = var.gcp_region
}


#Creating gke cluster. Using basic authentication to keep it simple.
resource "google_container_cluster" "gke-demo-cluster" {
    name = "gke-demo-cluster"
    network = "default"
    location = var.gcp_zone
    initial_node_count = 1
    node_config {
      machine_type = "n1-standard-4"
    }
    master_auth {
    username = "your cluster username here"
    password = "your cluster password here"
    client_certificate_config {
      issue_client_certificate = false
    }
    }
  
}

#Kubernetes provider to create the pods. 
#Ideally in production environments, either kubectl config file or certificate based authentication should be used.
provider "kubernetes" {
  host = "https://${google_container_cluster.gke-demo-cluster.endpoint}"
  username = google_container_cluster.gke-demo-cluster.master_auth.0.username
  password = google_container_cluster.gke-demo-cluster.master_auth.0.password
 // client_certificate = "${base64decode(google_container_cluster.gke-demo-cluster.master_auth.0.client_certificate)}"
  //client_key = "${base64decode(google_container_cluster.gke-demo-cluster.master_auth.0.client_key)}"
  cluster_ca_certificate = base64decode(google_container_cluster.gke-demo-cluster.master_auth.0.cluster_ca_certificate)
}
