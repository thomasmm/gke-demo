
#Creating persistent disk for mysql pods
resource "google_compute_disk" "mysql" {
  name = "wordpress-mysql"
  type = "pd-ssd"
  zone = var.gcp_zone
  size = 1
}

#MySQL PV
resource "kubernetes_persistent_volume" "mysql-pv" {
  metadata {
    name = "mysql-pv"
  }
  spec {
    capacity = {
      storage = "1Gi"
    }
    access_modes = ["ReadWriteMany"]
    storage_class_name = "pd-ssd"
    persistent_volume_source {
      gce_persistent_disk {
        pd_name = google_compute_disk.mysql.name
        fs_type = "ext4"
      }
    }
  }
}

#PVC for mysql
resource "kubernetes_persistent_volume_claim" "mysql-pvc" {
  metadata {
    name = "mysql-pv-claim"
    labels = {
      app = "wordpress"
    }
  }
  spec {
    access_modes = ["ReadWriteMany"]
    storage_class_name = "pd-ssd"
    resources {
      requests = {
        storage = "1Gi"
      }
    }
    volume_name = kubernetes_persistent_volume.mysql-pv.metadata[0].name
  }
}


#Creating GCE Persistent disk for wordpress files.
resource "google_compute_disk" "wordpress" {
  name = "wordpress"
  type = "pd-ssd"
  zone = var.gcp_zone
  size = 1
}

# PV for wordpress
resource "kubernetes_persistent_volume" "wordpress-pv" {
  metadata {
    name = "wordpress-pv"
  }
  spec {
    capacity = {
      storage = "1Gi"
    }
    access_modes = ["ReadWriteMany"]
    storage_class_name = "pd-ssd"
    persistent_volume_source {
      gce_persistent_disk {
        pd_name = google_compute_disk.wordpress.name
        fs_type = "ext4"
      }
    }
  }
}

resource "kubernetes_persistent_volume_claim" "wordpress" {
  metadata {
    name = "wp-pv-claim"
    labels = {
      app = "wordpress"
    }
  }
  spec {
    access_modes = ["ReadWriteMany"]
    storage_class_name = "pd-ssd"
    resources {
      requests = {
        storage = "1Gi"
      }
    }
    volume_name = kubernetes_persistent_volume.wordpress-pv.metadata[0].name
  }
}
