resource "kubernetes_service" "wordpress-svc" {
  metadata {
    name = "wordpress"
    labels = {
      app = "wordpress"
    }
  }
  spec {
    port {
      port        = 80
      target_port = 80
    }
    selector = {
      app  = "wordpress"
      tier = kubernetes_replication_controller.wordpress.spec[0].selector.tier
    }
    type = "LoadBalancer"
  }
}

output "lb_ip" {
  value = kubernetes_service.wordpress-svc.load_balancer_ingress[0].ip
}


# Replication controller with replica 2
resource "kubernetes_replication_controller" "wordpress" {
  metadata {
    name = "wordpress"
    labels = {
      app = "wordpress"
    }
  }
  spec {
    replicas = 2
    selector = {
      app  = "wordpress"
      tier = "frontend"
    }
    template {
      container {
        image = "wordpress:${var.wordpress_version}-apache"
        name  = "wordpress"

        env {
          name  = "WORDPRESS_DB_HOST"
          value = "mysql-wordpress"
        }
        env {
          name = "WORDPRESS_DB_PASSWORD"
          value_from {
            secret_key_ref {
              name = kubernetes_secret.mysql.metadata[0].name
              key  = "password"
            }
          }
        }

        port {
          container_port = 80
          name           = "wordpress"
        }

        volume_mount {
          name       = "wordpress-persistent-storage"
          mount_path = "/var/www/html"
        }
      }

      volume {
        name = "wordpress-persistent-storage"
        persistent_volume_claim {
          claim_name = kubernetes_persistent_volume_claim.wordpress.metadata[0].name
        }
      }
    }
  }
}

#Pod autoscaler to scale up to 4 replica if cpu goes beyond 80%
resource "kubernetes_horizontal_pod_autoscaler" "wordpress" {
  metadata {
    name = "wordpress-hpa"
  }
  spec {
    max_replicas = 4
    min_replicas = 2
    scale_target_ref {
      kind = "ReplicationController"
      name = "wordpress"
    }
    target_cpu_utilization_percentage = 80
  }
}
